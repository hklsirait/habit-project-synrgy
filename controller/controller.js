const { user, role, family } = require('../models')
const bcrypt = require('bcrypt')
var passport = require('passport')
const { render } = require('ejs')

class Controller {
  static register(req, res) {
    console.log(req.body)
    const email = req.body.email;
    const password = bcrypt.hashSync(req.body.password, 10);
    const roleId = 1;
    const username = req.body.username;
    const familyname = req.body.familyname;
    var familyId = 0

    family.create({
      familyname: familyname
    })
      .then(fam => {
        familyId = fam.id

        user.create({
          email: email,
          password: password,
          roleId: roleId,
          username: username,
          familyId: familyId
        })
          .then(ot => {
            // res.render("login");
            res.json({
              'status': 200,
              'message': 'berhasil mebuat data keluarga',
              'data': ot,
            });
          })
          .catch((err) => {
            res.render("register");
          })

      })
  }
  
  static addmember(req, res) {
    console.log(req.body)
    const username = req.body.username;
    const password = bcrypt.hashSync(req.body.password, 10);
    const roleId = 2;
    const familyId = req.user.familyId;

    user.create({
      username: username,
      password: password,
      roleId: roleId,
      familyId: familyId
    })
      .then((ot) => {
        res.json({
          'status': 200,
          'message': 'berhasil menambahkan data anak',
          'data': ot
        });

      })
      .catch((err) => {
        res.send(`Gagal menambahkan member anak, karena 
            ${JSON.stringify(err.message, null, 2)}`)
      })
  }

  static getMemberChild(req, res) {
    user.findAll({
      where: {
        familyId: req.user.familyId
      }
    })
      .then((fam) => {
        res.json({
          'status': 200,
          'message': 'berhasil get data child',
          'data': fam
        });

      });
  }

  static login(req, res) {
    console.log("login controller");
    console.log(req.body)
    passport.authenticate("local", {
      successRedirect: "/dashboard",
      failureRedirect: "/login"
      ,
    })(req, res);
  }
  static getDashboardPage(req, res) {
    console.log(req)
    console.log("===batas====")
    user.findAll({
      where: {
        // role: req.user.role,
        // family : req.user.familyId
      }
    })
      .then((task) => {
        res.render("dashboard")
      });
  }

  static getHomePage(req, res) {
    res.render("home", {
      data1: req.user.username,
    });
    res.json({
      'status': 200,
      'message': 'berhasil masuk ke child',
      'data': response
    });
  }

  static getHomeNotePage(req, res) {
    res.render("homenote", {
      data1: req.user.username,
    });

  }

  static getPointPage(req, res) {
    res.render("homepoint", {
      data1: req.user.username,
    });
    res.json({
      'status': 200,
      'message': 'berhasil masuk ke child',
      'data': response
    });
  }
  static getUpdateTaskPage(req, res) {
    res.render("homeupdatetask", {
      data1: req.user.username,
    });
    res.json({
      'status': 200,
      'message': 'berhasil masuk ke child',
      'data': response
    });
  }

  static logout(req, res) {
    req.logout();
    res.redirect("/");
  }
  static getIndexPage(req, res) {
    res.render("index");
  }
  static getRegisterPage(req, res) {
    res.render("register");
    res.json({
      'status': 200,
      'message': 'berhasil masuk ke register page'
    })
  }
  static getLoginPage(req, res) {
    res.render("login");
  }
  static getaddMemberPage(req, res) {
    res.render("addmember");
  }
}
module.exports = Controller;
