const { Note } = require('../models')
const { Task } = require('../models')
const { Category } = require('../models');
const { Rewards } = require('../models');

module.exports = {

    // NOTES
    getNote: (req, res) => {
        Note.findAll({
            where: {
                userId: req.user.id
            }
        })
            .then(response => {
                if (response !== 0) {
                    res.json({
                        'status': 200,
                        'message': 'berhasil',
                        'data': response
                    })
                } else {
                    res.json({
                        'status': 400,
                        'message': 'data tidak ditemukan',
                        'data': response
                    })
                }

            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'kesalahan server'
                })
            })
    },

    getDetailNote: (req, res) => {
        const noteId = req.params.id
        Note.findOne({
            where: {
                id: noteId
            }
        })
            .then(response => {
                res.json({
                    'status': 200,
                    'message': 'Success',
                    'data': response
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'kesalahan server'
                })
            })
    },

    createNote: (req, res) => {
        const { title, description, label, image, pin } = req.body;
        const userId = req.user.id
        Note.create({
            userId,
            title,
            description,
            label,
            image,
            pin
        })
            .then(notes => {
                res.json({
                    'status': 200,
                    'message': 'Success',
                    'data': notes
                })
            })
            .catch(err => {
                res.send(`Gagal menambahkan catatan, karena 
            ${JSON.stringify(err.message, null, 2)}`)
            })
    },

    updateNotes: (req, res) => {
        const query = {
            where: { id: req.params.id }
        }
        const { title, description, label, image, pin } = req.body;
        const userId = req.user.id
        Note.update({
            userId,
            title,
            description,
            label,
            image,
            pin
        }, query)
            .then(notes => {
                res.json({
                    'status': 200,
                    'message': 'Data berhasil diupdate',
                    'data': notes
                })
            })
            .catch(err => {
                console.log("Gagal update")
            })

    },

    deleteNotes: (req, res) => {
        const noteId = req.params.id
        Note.destroy({
            where: {
                id: noteId
            }
        })
            .then(response => {
                res.json({
                    'status': 200,
                    'message': 'Data berhasil dihapus',
                    'data': response
                })
            })
    },

    // TASK

    getTask: (req, res) => {
        Task.findAll({
            where: {
                userId: req.user.id
            }
        })
            .then(response => {
                if (response.length !== 0) {
                    res.json({
                        'status': 200,
                        'message': 'berhasil',
                        'data': response
                    })
                } else {
                    res.json({
                        'status': 400,
                        'message': 'data tidak ditemukan',
                    })
                }

            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'kesalahan server karena' + JSON.stringify(err.message, null, 2)
                })
            })
    },

    getDetailTask: (req, res) => {
        const taskId = req.params.id
        Task.findOne({
            where: {
                id: taskId
            }
        })
            .then(response => {
                res.json({
                    'status': 200,
                    'message': 'Success',
                    'data': response
                })
            })
    },

    createTask: (req, res) => {
        const { categoryId, title, deadline, point, completed, completedDate } = req.body;
        const userId = req.user.id
        Task.create({
            userId,
            categoryId,
            title,
            deadline,
            point,
            completed,
            completedDate
        })
            .then(task => {
                res.json({
                    'status': 200,
                    'message': 'Success',
                    'data': task
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            })
    },

    updateTask: (req, res) => {
        const query = {
            where: { id: req.params.id }
        }

        const { title, deadline, point, completed, completedDate } = req.body;
        Task.update({
            title,
            deadline,
            point,
            completed,
            completedDate
        }, query)
            .then(task => {
                res.json({
                    'status': 200,
                    'message': 'Data berhasil diupdate',
                    'data': task
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            })

    },

    deleteTask: (req, res) => {
        const taskId = req.params.id
        Task.destroy({
            where: {
                id: taskId
            }
        })
            .then(response => {
                res.json({
                    'status': 200,
                    'message': 'Data berhasil dihapus',
                    'data': response
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            })
    },

    searchTask: (req, res) => {
        const titleTask = req.params.title;
        Task.findOne({
            where: {
                title: titleTask
            }
        })
            .then(response => {
                if (response !== null) {
                    res.json({
                        'status': 200,
                        'message': 'Data berhasil ditemukan',
                        'data': response
                    })
                } else {
                    res.json({
                        'status': 400,
                        'message': 'data tidak ditemukan',
                    })
                }
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            })
    },

    // REWARD

    createReward: (req, res) => {
        const { namaReward, poin, claim } = req.body;
        const userId = req.user.id
        Rewards.create({
            namaReward,
            poin,
            claim,
            userId,
        })
            .then((response) => {
                res.json({
                    status: 200,
                    message: 'Success',
                    data: response,
                });
            })
            .catch((err) => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            });
    },

    getAllReward: (req, res) => {
        Rewards.findAll({
            where: {
                userId: req.user.id
            }
        })
            .then((response) => {
                if (response !== 0) {
                    res.json({
                        status: 200,
                        message: 'berhasil',
                        data: response,
                    });
                } else {
                    res.json({
                        status: 400,
                        message: 'data tidak ditemukan',
                        data: response,
                    });
                }
            })
            .catch((err) => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            });
    },

    getRewardById: (req, res) => {
        const rewardId = req.params.id;

        Rewards.findOne({
            where: {
                id: rewardId,
            },
        })
            .then((response) => {
                res.json({
                    status: 200,
                    message: 'Success',
                    data: response,
                });
            })
            .catch((err) => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            });
    },

    updateReward: (req, res) => {
        const query = {
            where: {
                id: req.params.id,
            },
        };
        const { namaReward, poin, claim } = req.body;
        const userId = req.user.id
        Rewards.update(
            {
                namaReward,
                poin,
                claim,
                userId,
            },
            query
        )
            .then((response) => {
                res.json({
                    status: 200,
                    message: 'Data berhasil diupdate',
                    data: response,
                });
            })
            .catch((err) => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            });
    },

    deleteReward: (req, res) => {
        const rewardId = req.params.id;
        Rewards.destroy({
            where: {
                id: rewardId,
            },
        }).then((response) => {
            res.json({
                status: 200,
                message: 'Data berhasil dihapus',
                data: response,
            });
        });
    },

    // Category
    getAllCategory: (req, res) => {
        Category.findAll({})
            .then(response => {
                if (response !== 0) {
                    res.json({
                        'status': 200,
                        'message': 'SUCCEED',
                        'data': response
                    })
                } else {
                    res.json({
                        'status': 400,
                        'message': 'NOT_FOUND',
                        'data': response
                    })
                }
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            })
    },

    getCategory: (req, res) => {
        const categoryId = req.params.id
        Category.findOne({
            where: {
                id: categoryId
            }
        })
            .then(response => {
                res.json({
                    'status': 200,
                    'message': 'SUCCEED',
                    'data': response
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            })
    },

    createCategory: (req, res) => {
        const { name, imageUrl } = req.body;
        Category.create({
            name,
            imageUrl
        })
            .then(category => {
                res.json({
                    'status': 200,
                    'message': 'SUCCEED',
                    'data': category
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            })
    },

    updateCategory: (req, res) => {
        const query = {
            where: { id: req.params.id }
        }

        const { name, imageUrl } = req.body;
        Category.update({
            name,
            imageUrl
        }, query)
            .then(notes => {
                res.json({
                    'status': 200,
                    'message': 'SUCCEED',
                    'data': notes
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            })

    },

    deleteCategory: (req, res) => {
        const categoryId = req.params.id
        Category.destroy({
            where: {
                id: categoryId
            }
        })
            .then(response => {
                res.json({
                    'status': 200,
                    'message': 'SUCCEED',
                    'data': response
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'Gagal karena' + " " + JSON.stringify(err.message, null, 2)
                })
            })
    }
}
