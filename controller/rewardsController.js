const { query } = require("express");

module.exports = {
  getRewardPage: (req, res) => {
    res.render("rewards");
  },
};
