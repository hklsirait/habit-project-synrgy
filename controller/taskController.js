const { Task } = require("../models");

module.exports = {
  getTaskPage: (req, res) => {
    res.render("tasks");
  },
};
