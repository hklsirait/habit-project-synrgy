const express = require('express')
const path = require('path')
const bcrypt = require('bcrypt')
const router = require('./routes')
const {user} = require('./models')
var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy
var session = require('express-session')
const app = express()

app.use(express.json());
app.use(express.urlencoded());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(
    session({
      secret: "secret",
      resave: true,
      saveUninitialized: true,
    })
  );

// passport config
passport.use(
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
      },
      (email, password, done) => {
        console.log('masuk passport')
        user.findOne({
            where: {
              email: email,
            },
            raw : true
          })
          .then((parent) => {
            console.log(parent);
            if (parent) {
              const encryptedPassword = parent.password
    
              if (bcrypt.compareSync(password, encryptedPassword)) {
                return done(null, parent)
              } else {
                
                return done(null, false)
              }
            } else {
              return done(null, false)
            }
          });
      }
    )
  );

  passport.serializeUser((user, done) => {
    done(null, user);
  });
  passport.deserializeUser((obj, done) => {
    done(null, obj);
  });

  //passport middleware
  app.use(passport.initialize());
  app.use(passport.session());

  app.use("/", router);
  app.use('/static', express.static('./views')); //untuk load path yang diperlukan di halaman

  app.use((err, req, res, next) => {
    if (err) {
      console.log(err);
    }
  });
  
  app.listen(3007, () => {
    console.log(`Example app listening at http://localhost:3007`);
  });
  
  module.exports = app;
