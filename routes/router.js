const express = require('express');
const router = express.Router();

const controller = require('../controller/controller');

var checkAuthentication = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect('/login');
  }
};

  router.get("/", controller.getIndexPage);
  router.get("/register", controller.getRegisterPage);
  router.get("/login", controller.getLoginPage);
  router.get("/dashboard", checkAuthentication, controller.getDashboardPage);
  router.get("/logout", controller.logout);
  router.get("/home", checkAuthentication, controller.getHomePage);
  router.get("/homenote", checkAuthentication, controller.getHomeNotePage);
  router.get("/homelistmember", controller.getMemberChild);
  router.get("/homepoint", checkAuthentication, controller.getPointPage);
  router.get("/homeupdatetask", checkAuthentication, controller.getUpdateTaskPage);
  router.get("/addmember", checkAuthentication, controller.getaddMemberPage);
  

  router.post("/register", controller.register);
  router.post("/login", controller.login);
  router.post("/addmember", controller.addmember);
  module.exports = router;