const router = require ("express").Router();

const controller = require ("../controller/noteController");

router.get('/',controller.getNotePage);

module.exports = router;