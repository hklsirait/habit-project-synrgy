const router = require ("express").Router();

const controller = require ("../controller/apiController");

//Endpoint notes
router.get('/notes/list',controller.getNote);
router.get('/notes/:id',controller.getDetailNote);
router.post('/notes/create',controller.createNote);
router.put("/notes/update/:id", controller.updateNotes);
router.delete("/notes/deleted/:id", controller.deleteNotes);

//Endpoint Task
router.get('/task/list',controller.getTask);
router.get('/task/:id',controller.getDetailTask);
router.post('/task/create',controller.createTask);
router.put("/task/update/:id", controller.updateTask);
router.delete("/task/delete/:id", controller.deleteTask);
router.get("/task/search/:title",controller.searchTask)

// Endpoint Reward
router.get('/rewards/list', controller.getAllReward);
router.get('/rewards/:id', controller.getRewardById);
router.post('/rewards/create', controller.createReward);
router.put('/rewards/:id', controller.updateReward);
router.delete('/rewards/:id', controller.deleteReward);

//Endpoit Category

router.get('/category/list',controller.getAllCategory);
router.get('/category/:id',controller.getCategory);
router.post('/category/create',controller.createCategory);
router.put("/category/:id", controller.updateCategory);
router.delete("/category/:id", controller.deleteCategory);


module.exports = router;