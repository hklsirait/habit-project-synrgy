const router = require('express').Router();

const rewardsController = require('../controller/rewardsController');

router.get('/', rewardsController.getRewardPage);

module.exports = router;
