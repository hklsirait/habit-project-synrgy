const router = require("express").Router();
const userRouter = require("../routes/router");
const taskRouter = require("../routes/taskRouter");
const noteRouter = require("../routes/noteRouter");
const rewardsRouter = require("../routes/rewardsRoutes");
const apiRouter = require("../routes/apiRouter");

router.use("/", userRouter);
router.use("/api", apiRouter);
router.use("/tasks", taskRouter);
router.use("/note", noteRouter);
router.use("/rewards", rewardsRouter);

module.exports = router;
