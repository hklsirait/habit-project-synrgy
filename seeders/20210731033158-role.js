'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
     
      await queryInterface.bulkInsert('roles', [{
        rolename: 'parent',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        rolename: 'child',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        rolename: 'admin',
        createdAt: new Date(),
        updatedAt: new Date()
      }

    ], {});
    
  },

  down: async (queryInterface, Sequelize) => {
    
      
     
     
      await queryInterface.bulkDelete('roles', null, {});
     
  }
};
