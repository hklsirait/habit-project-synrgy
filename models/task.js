'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Task extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Task.belongsTo(models.user)
    }
  };
  Task.init({
    userId: DataTypes.BIGINT,
    categoryId: DataTypes.BIGINT,
    title: DataTypes.STRING,
    deadline: DataTypes.DATE,
    point: DataTypes.INTEGER,
    completed: DataTypes.BOOLEAN,
    completedDate: DataTypes.DATE,
    asignFor: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Task',
  });
  return Task;
};