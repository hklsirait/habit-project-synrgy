'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Note extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Note.belongsTo(models.user)
    }
  };
  Note.init({
    userId: DataTypes.BIGINT,
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    label: DataTypes.STRING,
    image: DataTypes.STRING,
    pin: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Note',
  });
  return Note;
};