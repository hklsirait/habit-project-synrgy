'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reward extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Reward.belongsTo(models.user, { foreignKey: 'userId' });
    }
  }
  Reward.init(
    {
      namaReward: DataTypes.STRING,
      poin: DataTypes.INTEGER,
      claim: DataTypes.BOOLEAN,
      userId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'Rewards',
    }
  );
  return Reward;
};
